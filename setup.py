# -*- coding: utf-8 -*-

from setuptools import setup
    
from wheel.bdist_wheel import bdist_wheel as _bdist_wheel
class bdist_wheel(_bdist_wheel):
    def finalize_options(self):
        _bdist_wheel.finalize_options(self)
        self.root_is_pure = False
            
readme = open('README.rst', 'r')
README_TEXT = readme.read()
readme.close()

    
setup(
    name='exiv2',
    version='0.3.1',
    packages=['exiv2'],
    python_requires='>=3.4',
    description='Exiv2 wrapper for Python >= 3.4 on Windows',
    long_description = README_TEXT,    
    url='https://bitbucket.org/zmic/exiv2-python',
    author='Michael Vanslembrouck',
    license='MIT',
    package_data = {
        # If any package contains *.txt or *.rst files, include them:
        '': ['example/*.py', 'test/*.py', 'test/test.jpg', 'test/test.png', 'cexiv2.pyd'],
    },    
    cmdclass={'bdist_wheel': bdist_wheel},    
    classifiers=[
        'Development Status :: 4 - Beta',
    ]        
)
      
      