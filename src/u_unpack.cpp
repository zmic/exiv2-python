#include "precompile.h"
#include "u_util.h"
#include "u_unpack.h"

bool unpack_bytes(PyObject* o, std::string& out)
{
    if (PyBytes_Check(o))
    {
        char* s;
        Py_ssize_t nbr_bytes;
        PyBytes_AsStringAndSize(o, &s, &nbr_bytes);
        out.assign(s, nbr_bytes);
        return true;
    }
    set_python_error(PyExc_TypeError, "Expected bytes");
    return nullptr;
}
