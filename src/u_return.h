#pragma once

template<typename T>
inline PyObject* py_return(T& t)
{
    compile_error;
}

template<typename T>
inline PyObject* py_return(T& t, PyObject* owner)
{
    compile_error;
}

#if 0
template<typename T>
inline PyObject* py_return(const T& t)
{
    compile_error;
}
#endif

template<>
inline PyObject* py_return(void* & p)
{
    return PyLong_FromVoidPtr(p);
}

template<>
inline PyObject* py_return(bool& t)
{
    return PyBool_FromLong(t);
}

template<>
inline PyObject* py_return(uint16_t& t)
{
    return PyLong_FromLong(t);
}

template<>
inline PyObject* py_return(int& t)
{
    return PyLong_FromLong(t);
}

template<>
inline PyObject* py_return(long& t)
{
    return PyLong_FromLong(t);
}

template<>
inline PyObject* py_return(size_t& t)
{
    return PyLong_FromSize_t(t);
}

template<>
inline PyObject* py_return(float& t)
{
    return PyFloat_FromDouble(double(t));
}

template<>
inline PyObject* py_return(const char*& s)
{
    if (s)
    {
        return PyBytes_FromString(s);
    }
    Py_RETURN_NONE;
}

template<>
inline PyObject* py_return(std::string& t)
{
    return PyBytes_FromStringAndSize(t.c_str(), t.length());
}

template<>
inline PyObject* py_return(Exiv2::Rational& p)
{
    return Py_BuildValue("ii", p.first, p.second);
}

template<class T>
inline PyObject* PyLong_FromConstVoidPtr(const T* p)
{
    return PyLong_FromVoidPtr(const_cast<void*>(static_cast<const void*>(p)));
}

template<>
inline PyObject* py_return(Exiv2::TypeId& t)
{
    return PyLong_FromLong(t);
}

template<>
inline PyObject* py_return(Exiv2::ExifData::iterator &p)
{
    // typedef std::list<Exifdatum> ExifMetadata;
    auto copy = new Exiv2::ExifData::iterator(p);
    return PyLong_FromVoidPtr(static_cast<void*>(copy));
}

template<>
inline PyObject* py_return(Exiv2::ExifData &p, PyObject* owner)
{
    return baseclass<Exiv2::ExifData>::alloc(&p, owner);
}

template<>
inline PyObject* py_return(Exiv2::IptcData &p, PyObject* owner)
{
    return baseclass<Exiv2::IptcData>::alloc(&p, owner);
}

template<>
inline PyObject* py_return(Exiv2::XmpData &p, PyObject* owner)
{
    return baseclass<Exiv2::XmpData>::alloc(&p, owner);
}

template<>
inline PyObject* py_return(Exiv2::Exifdatum &p, PyObject* owner)
{
    return baseclass<Exiv2::Exifdatum>::alloc(&p, owner);
}

template<>
inline PyObject* py_return(Exiv2::Iptcdatum &p, PyObject* owner)
{
    return baseclass<Exiv2::Iptcdatum>::alloc(&p, owner);
}

template<>
inline PyObject* py_return(Exiv2::Xmpdatum &p, PyObject* owner)
{
    return baseclass<Exiv2::Xmpdatum>::alloc(&p, owner);
}

template<>
inline PyObject* py_return(Exiv2::Value& p, PyObject* owner)
{
    return baseclass<Exiv2::Value>::alloc(&p, owner);
}

template<>
inline PyObject* py_return(const Exiv2::Value& p, PyObject* owner)
{
    return baseclass<Exiv2::Value>::alloc(&p, owner);
}

template<>
inline PyObject* py_return(Exiv2::BasicIo &p, PyObject* owner)
{
    return baseclass<Exiv2::BasicIo>::alloc(&p, owner);
}







