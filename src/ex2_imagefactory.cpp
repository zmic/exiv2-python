#include "precompile.h"
#include "main.h"

METHOD_TABLE(ImageFactory)

VARARGS(ImageFactory, open)
{
	bool use_curl = false;
	const Py_UNICODE* s;
	const Exiv2::byte *data;
	long size;
    Exiv2::Image::AutoPtr ptr;
	if (PyArg_ParseTuple(args, "u", &s))
	{
		UNLOCK_GIL unlock;
		std::wstring path(s);
		ptr = Exiv2::ImageFactory::open(path, use_curl);
	}
	else
	{
		PyErr_Clear();
		if (PyArg_ParseTuple(args, "y#", &data, &size))
		{
			UNLOCK_GIL unlock;
			ptr = Exiv2::ImageFactory::open(data, size);
		}
		else
		{
			return nullptr;
		}
	}
	//return PyLong_FromVoidPtr(ptr.release());
    return baseclass<Exiv2::Image>::alloc(ptr.release(), nullptr);
}

