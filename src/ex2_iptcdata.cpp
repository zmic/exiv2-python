#include "precompile.h"
#include "main.h"

CLASS_METHOD_TABLE(IptcData);
CLASS_METHOD_TABLE(Iptcdatum);

METHOD_VOID_NOARGS(IptcData, clear)
METHOD_VOID_NOARGS(IptcData, sortByKey)
METHOD_VOID_NOARGS(IptcData, sortByTag)

METHOD_GET_NOARGS(IptcData, empty)
METHOD_GET_NOARGS(IptcData, count)
METHOD_GET_NOARGS(IptcData, size)
METHOD_GET_NOARGS(IptcData, detectCharset)

METHOD_SET(IptcData, add)

//METHOD_GET_NOARGS_NONCONST(IptcData, begin)
//METHOD_GET_NOARGS_NONCONST(IptcData, end)

/*
typedef struct {
    lenfunc mp_length;
    binaryfunc mp_subscript;
    objobjargproc mp_ass_subscript;
} PyMappingMethods;
*/

Py_ssize_t IptcData_length(PyObject* self)
{
    auto p = unpack_self<IptcData>(self);
    if (p)
    {
        return p->count();
    }
    return -1;
}

PyObject* IptcData_subscript(PyObject* self, PyObject* index)
{
    std::string key;

    auto p = unpack_self<IptcData>(self);
    if (p)
    {
        if (unpack_bytes(index, key))
        {
            Exiv2::Iptcdatum& datum = p->operator[](key);
            return py_return(datum, self);
        }
    }
    return nullptr;
}

// return -1 on failure; return 0 on success.
int IptcData_subscript_assignment(PyObject* self, PyObject* index, PyObject* value)
{
    std::string key;
    auto p = unpack_self<IptcData>(self);
    if (p)
    {
        if (unpack_bytes(index, key))
        {
            if (value)
            {
                Exiv2::Iptcdatum& datum = p->operator[](key);
                if (unpack_datum_assignment<Iptcdatum>(&datum, value))
                {
                    return 0;
                }
            }
            else
            {
                auto it = p->findKey(Exiv2::IptcKey(key));
                if (it != p->end())
                {
                    p->erase(it);
                    return 0;
                }
                set_python_error(PyExc_KeyError, "%s", key.c_str());
            }
        }
    }
    return -1;
}




PyObject* IptcData_getiter(IptcData* self)
{
    IptcData::exiv2type* p = self->_p;
    return iterator<IptcData::exiv2type::iterator>::alloc((PyObject*)self, p->begin(), p->end());
}

PyMappingMethods IptcData_mapping_methods[] = {
    [](PyObject* self) { return exiv2_catcher(IptcData_length, self); },
    [](PyObject* self, PyObject* args) { return exiv2_catcher(IptcData_subscript, self, args); },
    [](PyObject* self, PyObject* obj, PyObject* args) { return exiv2_catcher(IptcData_subscript_assignment, self, obj, args); }
};

int f()
{
    baseclass<Exiv2::IptcData>::type_object.tp_new = _new<Exiv2::IptcData>;
    baseclass<Exiv2::IptcData>::type_object.tp_iter = (getiterfunc)IptcData_getiter;
    baseclass<Exiv2::IptcData>::type_object.tp_as_mapping = IptcData_mapping_methods;

    baseclass<Exiv2::Iptcdatum>::type_object.tp_new = new_buffer_buffer<Exiv2::Iptcdatum, Exiv2::IptcKey>;

    return 0;
}



static int dummy = f();

#if 0
int add(const IptcKey& key, Value* value);
/*!
@brief Add a copy of the Iptcdatum to the IPTC metadata. A check
for non-repeatable datasets is performed.
@return 0 if successful;<BR>
6 if the dataset already exists and is not repeatable;<BR>
*/
int add(const Iptcdatum& iptcdatum);
/*!
@brief Delete the Iptcdatum at iterator position pos, return the
position of the next Iptcdatum. Note that iterators into
the metadata, including pos, are potentially invalidated
by this call.
*/
iterator erase(iterator pos);
/*!
@brief Delete all Iptcdatum instances resulting in an empty container.
*/
//! Begin of the metadata
iterator begin() { return iptcMetadata_.begin(); }
//! End of the metadata
iterator end() { return iptcMetadata_.end(); }
/*!
@brief Find the first Iptcdatum with the given key, return an iterator
to it.
*/
iterator findKey(const IptcKey& key);
/*!
@brief Find the first Iptcdatum with the given record and dataset it,
return a const iterator to it.
*/
iterator findId(uint16_t dataset,
    uint16_t record = IptcDataSets::application2);
//@}

//! @name Accessors
//@{
//! Begin of the metadata
const_iterator begin() const { return iptcMetadata_.begin(); }
//! End of the metadata
const_iterator end() const { return iptcMetadata_.end(); }
/*!
@brief Find the first Iptcdatum with the given key, return a const
iterator to it.
*/
const_iterator findKey(const IptcKey& key) const;
/*!
@brief Find the first Iptcdatum with the given record and dataset
number, return a const iterator to it.
*/
const_iterator findId(uint16_t dataset,
    uint16_t record = IptcDataSets::application2) const;
/*!
@brief dump iptc formatted binary data (used by printStructure kpsRecursive)
*/
static void printStructure(std::ostream& out, const byte* bytes, const size_t size, uint32_t depth);
//@}
#endif


typedef Exiv2::Iptcdatum xxx;

METHOD_GET_NOARGS(Iptcdatum, recordName)
METHOD_GET_NOARGS(Iptcdatum, groupName)
METHOD_GET_NOARGS(Iptcdatum, familyName)
METHOD_GET_NOARGS(Iptcdatum, tagName)
METHOD_GET_NOARGS(Iptcdatum, tagLabel)
METHOD_GET_NOARGS(Iptcdatum, key)
METHOD_GET_NOARGS(Iptcdatum, record)
METHOD_GET_NOARGS(Iptcdatum, tag)
METHOD_GET_NOARGS(Iptcdatum, typeId)
METHOD_GET_NOARGS(Iptcdatum, typeName)
METHOD_GET_NOARGS(Iptcdatum, typeSize)
METHOD_GET_NOARGS(Iptcdatum, count)
METHOD_GET_NOARGS(Iptcdatum, size)

METHOD_GETREF_NOARGS_CONST(Iptcdatum, value)

METHOD_GET_OPTIONAL(Iptcdatum, toString)
METHOD_GET_OPTIONAL(Iptcdatum, toLong)
METHOD_GET_OPTIONAL(Iptcdatum, toFloat)
METHOD_GET_OPTIONAL(Iptcdatum, toRational)




METHOD_SET_VALUE(Iptcdatum)

#if 0
METHOD_VARARGS(Iptcdatum, setValue)
{
    auto p = unpack_self<Iptcdatum>(self);
    if (p)
    {
        if (unpack_datum_assignment_args<Iptcdatum>(p, args))
        {
            Py_INCREF(self);
            return self;
        }
    }
    return nullptr;
}
#endif


#if 0
    public:
        //! @name Creators
        //@{
        /*!
        @brief Constructor for new tags created by an application. The
        %Iptcdatum is created from a key / value pair. %Iptcdatum
        copies (clones) the value if one is provided. Alternatively, a
        program can create an 'empty' %Iptcdatum with only a key and
        set the value using setValue().

        @param key The key of the %Iptcdatum.
        @param pValue Pointer to a %Iptcdatum value.
        @throw Error if the key cannot be parsed and converted
        to a tag number and record id.
        */
        explicit Iptcdatum(const IptcKey& key,
            const Value* pValue = 0);
        //! Copy constructor
        Iptcdatum(const Iptcdatum& rhs);
        //! Destructor
        virtual ~Iptcdatum();
        //@}

        //! @name Manipulators
        //@{
        //! Assignment operator
        Iptcdatum& operator=(const Iptcdatum& rhs);
        /*!
        @brief Assign \em value to the %Iptcdatum. The type of the new Value
        is set to UShortValue.
        */
        Iptcdatum& operator=(const uint16_t& value);
        /*!
        @brief Assign \em value to the %Iptcdatum.
        Calls setValue(const std::string&).
        */
        Iptcdatum& operator=(const std::string& value);
        /*!
        @brief Assign \em value to the %Iptcdatum.
        Calls setValue(const Value*).
        */
        Iptcdatum& operator=(const Value& value);
        void setValue(const Value* pValue);
        /*!
        @brief Set the value to the string \em value, using
        Value::read(const std::string&).
        If the %Iptcdatum does not have a Value yet, then a %Value of
        the correct type for this %Iptcdatum is created. If that
        fails (because of an unknown dataset), a StringValue is
        created. Return 0 if the value was read successfully.
        */
        int setValue(const std::string& value);
        //@}

        //! @name Accessors
        //@{
        long copy(byte* buf, ByteOrder byteOrder) const;
        std::ostream& write(std::ostream& os, const ExifData* pMetadata = 0) const;
        /*!
        @brief Return the key of the Iptcdatum. The key is of the form
        '<b>Iptc</b>.recordName.datasetName'. Note however that the key
        is not necessarily unique, i.e., an IptcData object may contain
        multiple metadata with the same key.
        */
        std::string key() const;
        /*!
        @brief Return the record id
        @return record id
        */
        uint16_t record() const;
        //! Return the tag (aka dataset) number
        uint16_t tag() const;
        TypeId typeId() const;
        const char* typeName() const;
        long typeSize() const;
        long count() const;
        long size() const;
        std::string toString() const;
        std::string toString(long n) const;
        long toLong(long n = 0) const;
        float toFloat(long n = 0) const;
        Rational toRational(long n = 0) const;


        Value::AutoPtr getValue() const;
        const Value& value() const;
        //@}
#endif

#if 0
METHOD_TABLE(Iptcdatum)

/////////////////////////////////////////////////////////////////////////////////////////////

VARARGS(Iptcdatum, setValue)
{
    std::string value;
    Iptcdatum* p = unpack_bytes<Iptcdatum>(args, value);
    if (p)
    {
        int result = p->setValue(value);
        return PyLong_FromLong(result);
    }
    return nullptr;
}

/////////////////////////////////////////////////////////////////////////////////////////////

VARARGS(Iptcdatum, value)
{
    Iptcdatum* d = unpack_noargs<Iptcdatum>(args);
    if (d)
    {
        const Value* ptr = &(d->value());
        return PyLong_FromVoidPtr((void*)ptr);
    }
    return nullptr;
}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////

#if 0
PyObject* f1(PyObject* self, PyObject* args, std::string(Iptcdatum::*pf)(void) const)
{
    Iptcdatum* d = unpack_noargs<Iptcdatum>(args);
    if (d)
    {
        std::string s((d->*pf)());
        return PyUnicode_FromStringAndSize(s.c_str(), s.length());
    }
    return nullptr;
}

//TRAMPOLINE(f1, Iptcdatum, key)
//TRAMPOLINE(f1, Iptcdatum, recordName)
//TRAMPOLINE(f1, Iptcdatum, groupName)
//TRAMPOLINE(f1, Iptcdatum, tagName)
//TRAMPOLINE(f1, Iptcdatum, tagLabel)
//TRAMPOLINE(f1, Iptcdatum, toString)

GET_NOARGS(Iptcdatum, recordName)
GET_NOARGS(Iptcdatum, groupName)
GET_NOARGS(Iptcdatum, tagName)
GET_NOARGS(Iptcdatum, tagLabel)
GET_NOARGS(Iptcdatum, key)
GET_NOARGS(Iptcdatum, toString)

#endif


