#pragma once

template <typename ITERATORTYPE>
struct iterator
{
    typedef typename ITERATORTYPE iteratortype;

    PyObject_HEAD;
    PyObject* _owner;
    ITERATORTYPE _walker;
    ITERATORTYPE _end;

    static PyObject* alloc(PyObject* owner, ITERATORTYPE begin, ITERATORTYPE end)
    {
        iterator* o = (iterator*)(type_object.tp_alloc(&type_object, 0));
        Py_INCREF(owner);
        new (&o->_walker) ITERATORTYPE;
        new (&o->_end) ITERATORTYPE;
        o->_owner = owner;
        o->_walker = begin;
        o->_end = end;
        return (PyObject*)o;
    }

    static void dealloc(iterator* self)
    {
        self->_end.~ITERATORTYPE();
        self->_walker.~ITERATORTYPE();
        Py_DECREF(self->_owner);
        Py_TYPE(self)->tp_free((PyObject*)self);
        //type_object.tp_free((PyObject*)self);
    }

    static PyObject* getiter(PyObject* self)
    {
        Py_INCREF(self);
        return self;
    }

    static PyObject* iternext(iterator* self)
    {
        if (self->_walker != self->_end)
        {
            return py_return(*self->_walker++, (PyObject*)self);
        }
        set_python_error(PyExc_StopIteration, "");
        return nullptr;
    }

    static PyTypeObject type_object;
};

template<class ITERATORTYPE>
PyTypeObject iterator<ITERATORTYPE>::type_object =
{
    PyVarObject_HEAD_INIT(NULL, 0)
    "cexiv2.iterator",                          // const char *tp_name; /* For printing, in format "<module>.<name>" */
    sizeof(iterator),                           // Py_ssize_t tp_basicsize; /* For allocation */
    0,                                          // Py_ssize_t tp_itemsize; /* For allocation */

                                                // /* Methods to implement standard operations */

    (destructor)dealloc,                        // destructor tp_dealloc;
    0,                                          // printfunc tp_print;
    0,                                          // getattrfunc tp_getattr;
    0,                                          // setattrfunc tp_setattr;
    0,                                          // void *tp_reserved; /* formerly known as tp_compare */
    0,                                          // reprfunc tp_repr;

                                                // /* Method suites for standard classes */

    0,                                          // PyNumberMethods *tp_as_number;
    0,                                          // PySequenceMethods *tp_as_sequence;
    0,                                          // PyMappingMethods *tp_as_mapping;

                                                // /* More standard operations (here for binary compatibility) */

    0,                                          // hashfunc tp_hash;
    0,                                          // ternaryfunc tp_call;
    0,                                          // reprfunc tp_str;
    0,                                          // getattrofunc tp_getattro;
    0,                                          // setattrofunc tp_setattro;

                                                // /* Functions to access object as input/output buffer */
    0,                                          // PyBufferProcs *tp_as_buffer;

                                                // /* Flags to define presence of optional/expanded features */
    Py_TPFLAGS_DEFAULT,                         // long tp_flags;

    0,                                          // const char *tp_doc; /* Documentation string */

                                                // /* Assigned meaning in release 2.0 */
                                                // /* call function for all accessible objects */
    0,                                          // traverseproc tp_traverse;

                                                // /* delete references to contained objects */
    0,                                          // inquiry tp_clear;

                                                // /* Assigned meaning in release 2.1 */
                                                // /* rich comparisons */
    0,                                          // richcmpfunc tp_richcompare;

                                                // /* weak reference enabler */
    0,                                          // Py_ssize_t tp_weaklistoffset;

                                                // /* Iterators */
    (getiterfunc)getiter,                       // getiterfunc tp_iter;
    (iternextfunc)iternext,                     // iternextfunc tp_iternext;

                                                // /* Attribute descriptor and subclassing stuff */
    0,                                          // struct PyMethodDef *tp_methods;
    0,                                          // struct PyMemberDef *tp_members;
    0,                                          // struct PyGetSetDef *tp_getset;
    0,                                          // struct _typeobject *tp_base;
    0,                                          // PyObject *tp_dict;
    0,                                          // descrgetfunc tp_descr_get;
    0,                                          // descrsetfunc tp_descr_set;
    0,                                          // Py_ssize_t tp_dictoffset;
    0,                                          // initproc tp_init;
    0,                                          // allocfunc tp_alloc;
    0,                                          // newfunc tp_new;
    0,                                          // freefunc tp_free; /* Low-level free-memory routine */
    0,                                          // inquiry tp_is_gc; /* For PyObject_IS_GC */
    0,                                          // PyObject *tp_bases;
    0,                                          // PyObject *tp_mro; /* method resolution order */
    0,                                          // PyObject *tp_cache;
    0,                                          // PyObject *tp_subclasses;
    0,                                          // PyObject *tp_weaklist;
    0,                                          // destructor tp_del;

                                                // /* Type attribute cache version tag. Added in version 2.6 */
    0                                           // unsigned int tp_version_tag;
};

