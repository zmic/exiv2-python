#pragma once

int push_NOARGS(std::vector<PyMethodDef>& methods, const char* name, PyObject* (*pf)(PyObject*), const char* docstring = nullptr);
int push_VARARGS(std::vector<PyMethodDef>& methods, const char* name, PyObject* (*pf)(PyObject*, PyObject*), const char* docstring = nullptr);

struct wrapped_method
{
    wrapped_method(std::vector<PyMethodDef>& methods, const char* pyname, PyObject* (*pf)(PyObject*))
    {
        push_NOARGS(methods, pyname, pf);
    }
    wrapped_method(std::vector<PyMethodDef>& methods, const char* pyname, PyObject* (*pf)(PyObject*, PyObject*))
    {
        push_VARARGS(methods, pyname, pf);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////


#define METHOD_PUSH_NOARGS__(cls, cfunc, pyname)\
namespace PYNS_##cls##pyname\
{\
	auto lambda = [](PyObject* self) -> PyObject* { return exiv2_catcher(cfunc, self); };\
    wrapped_method method(cls::methods, #pyname, lambda);\
}

#define METHOD_NOARGS(klass, method)\
PyObject* py_##klass##_##method(PyObject* self);\
METHOD_PUSH_NOARGS__(klass, py_##klass##_##method, klass##_##method)\
PyObject* py_##klass##_##method(PyObject* self)

////////////////////////////////////////////////////////////////////////////////////////////////////


#define METHOD_PUSH_VARARGS(cls, cfunc, pyname)\
namespace PYNS_##cls##pyname\
{\
	auto lambda = [](PyObject* self, PyObject* args) -> PyObject* { return exiv2_catcher(cfunc, self, args); };\
    wrapped_method method(cls::methods, #pyname, lambda);\
}


#define METHOD_VARARGS(klass, method)\
PyObject* py_##klass##_##method(PyObject* self, PyObject* args);\
METHOD_PUSH_VARARGS(klass, py_##klass##_##method, method)\
PyObject* py_##klass##_##method(PyObject* self, PyObject* args)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, void(T::exiv2type::*pf)(void)>
PyObject* method_void_noargs(PyObject* self)
{
    T::exiv2type* _this = unpack_self<T>(self);
    if (_this)
    {
        (_this->*pf)();
        Py_RETURN_NONE;
    }
    return nullptr;
}

#define METHOD_VOID_NOARGS__(klass, method) method_void_noargs<klass, &klass::exiv2type::method>

#define METHOD_VOID_NOARGS(klass, method)\
METHOD_PUSH_NOARGS__(klass, METHOD_VOID_NOARGS__(klass, method), method)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, typename PFTYPE, PFTYPE pf>
PyObject* method_get_noargs(PyObject* self)
{
    T::exiv2type* _this = unpack_self<T>(self);
    if (_this)
    {
        auto r = (_this->*pf)();
        return py_return(r);
    }
    return nullptr;
}

#define METHOD_GET_NOARGS__(klass, pftype, ptrfunc) method_get_noargs<klass, pftype, ptrfunc>

#define METHOD_GET_NOARGS(klass, method)\
METHOD_PUSH_NOARGS__(klass, METHOD_GET_NOARGS__(klass, decltype(&klass::exiv2type::method), &klass::exiv2type::method), method)

#define METHOD_GET_NOARGS_CONST(klass, method)\
typedef decltype(((const klass::exiv2type*)nullptr)->method()) klass##method##const_returntype;\
auto klass##method##ptr_func = static_cast<klass##method##const_returntype(klass::exiv2type::*)() const >(&klass::exiv2type::method);\
METHOD_PUSH_NOARGS__(klass, METHOD_GET_NOARGS__(klass, const decltype(klass##method##ptr_func)&, klass##method##ptr_func), method)

#define METHOD_GET_NOARGS_NONCONST(klass, method)\
typedef decltype(((klass::exiv2type*)nullptr)->method()) klass##method##const_returntype;\
auto klass##method##ptr_func = static_cast<klass##method##const_returntype(klass::exiv2type::*)()>(&klass::exiv2type::method);\
METHOD_PUSH_NOARGS__(klass, METHOD_GET_NOARGS__(klass, decltype(klass##method##ptr_func)&, klass##method##ptr_func), method)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, typename PFTYPE, PFTYPE pf>
PyObject* method_getref_noargs(PyObject* self)
{
    T::exiv2type* _this = unpack_self<T>(self);
    if (_this)
    {
        auto& r = (_this->*pf)();
        return py_return(r, self);
    }
    return nullptr;
}

#define METHOD_GETREF_NOARGS__(klass, pftype, ptrfunc) method_getref_noargs<klass, pftype, ptrfunc>

#define METHOD_GETREF_NOARGS(klass, method)\
METHOD_PUSH_VARARGS(klass, METHOD_GETREF_NOARGS__(klass, decltype(&klass::exiv2type::method), &klass::exiv2type::method), method)

#define MEMBER_NONCONST(klass, method) static_cast<decltype(((klass*)nullptr)->method())(klass::*)()>(&klass::method);
#define MEMBER_CONST(klass, method) static_cast<decltype(((const klass*)nullptr)->method())(klass::*)() const>(&klass::method);

#define METHOD_GETREF_NOARGS_NONCONST(klass, method)\
auto klass##method##ptr_func = MEMBER_NONCONST(klass::exiv2type, method);\
METHOD_PUSH_NOARGS__(klass, METHOD_GETREF_NOARGS__(klass, decltype(klass##method##ptr_func)&, klass##method##ptr_func), method)

#define METHOD_GETREF_NOARGS_CONST(klass, method)\
auto klass##method##ptr_func = MEMBER_CONST(klass::exiv2type, method);\
METHOD_PUSH_NOARGS__(klass, METHOD_GETREF_NOARGS__(klass, decltype(klass##method##ptr_func)&, klass##method##ptr_func), method)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, typename R, typename U>
PyObject* method_get_optional__(PyObject* self, PyObject* args, R(T::exiv2type::*pf)(U) const)
{
    U u = U();
    T::exiv2type* _this = unpack_optional<T>(self, args, u);
    if (_this)
    {
        auto r = (_this->*pf)(u);
        return py_return(r);
    }
    return nullptr;
}

#define METHOD_PUSH_GET_OPTIONAL__(cls, cfunc, pyname)\
namespace PYNS_##cls##pyname##0 {\
    auto lambda2 = [](PyObject* self, PyObject* args) -> PyObject* { return method_get_optional__<cls>(self, args, cfunc); }; \
    METHOD_PUSH_VARARGS(cls, lambda2, pyname)\
}

#define METHOD_GET_OPTIONAL(klass, method)\
METHOD_PUSH_GET_OPTIONAL__(klass, &klass::exiv2type::method, method)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, typename R, typename U>
PyObject* method_set(PyObject* self, PyObject* args, R(T::exiv2type::*pf)(const U&))
{
    const U* u;
    T::exiv2type* _this = unpack<T, const U*>(self, args, u);
    if (_this)
    {
        auto r = (_this->*pf)(*u);
        return py_return(r);
    }
    return nullptr;
}

#define METHOD_PUSH_SET__(cls, cfunc, pyname)\
namespace PYNS_##cls##pyname##0 {\
    auto lambda2 = [](PyObject* self, PyObject* args) -> PyObject* { return method_set<cls>(self, args, cfunc); }; \
    METHOD_PUSH_VARARGS(cls, lambda2, pyname)\
}

#define METHOD_SET(klass, method)\
METHOD_PUSH_SET__(klass, &klass::exiv2type::method, method)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, typename U>
PyObject* void_method_set(PyObject* self, PyObject* args, void(T::exiv2type::*pf)(const U&))
{
    const U* u;
    T::exiv2type* _this = unpack<T, const U*>(self, args, u);
    if (_this)
    {
        (_this->*pf)(*u);
        Py_RETURN_NONE;
    }
    return nullptr;
}

#define VOID_METHOD_PUSH_SET__(cls, cfunc, pyname)\
namespace PYNS_##cls##pyname##0 {\
    auto lambda2 = [](PyObject* self, PyObject* args) -> PyObject* { return void_method_set<cls>(self, args, cfunc); }; \
    METHOD_PUSH_VARARGS(cls, lambda2, pyname)\
}

#define VOID_METHOD_SET(klass, method)\
VOID_METHOD_PUSH_SET__(klass, &klass::exiv2type::method, method)

////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename T>
PyObject* method_set_value__(typename T::exiv2type* assignee, PyObject* arg)
{
    if (PyBytes_Check(arg))
    {
        std::string str;
        char* s;
        Py_ssize_t nbr_bytes;
        PyBytes_AsStringAndSize(arg, &s, &nbr_bytes);
        str.assign(s, nbr_bytes);
        int return_value = assignee->setValue(str);
        return PyLong_FromLong(return_value);
    }
    else if (PyLong_Check(arg))
    {
        *assignee = uint16_t(PyLong_AsLong(arg));
        Py_RETURN_NONE;
    }
    else if (PyObject_IsInstance(arg, (PyObject*)&T::type_object))
    {
        *assignee = *(((T*)arg)->_p);
        Py_RETURN_NONE;
    }
    else if (PyObject_IsInstance(arg, (PyObject*)&baseclass<Exiv2::Value>::type_object))
    {
        assignee->setValue((((baseclass<Exiv2::Value>*)arg)->_p));
        Py_RETURN_NONE;
    }
    set_python_error(PyExc_TypeError, "Value should be bytes, or int, or %s, or %s", T::type_object.tp_name, baseclass<Exiv2::Value>::type_object.tp_name);
    return nullptr;
}

template<typename T>
PyObject* method_set_value(PyObject* self, PyObject* args)
{
    PyObject* arg;
    T::exiv2type* _this = unpack<T, PyObject*>(self, args, arg);
    if (_this)
    {
        return method_set_value__<T>(_this, arg);
    }
    return nullptr;

}

#define METHOD_SET_VALUE(cls)\
METHOD_PUSH_VARARGS(cls, method_set_value<cls>, setValue)

