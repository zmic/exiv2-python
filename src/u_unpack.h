#pragma once

template<typename T>
inline typename T::exiv2type* unpack_self(PyObject* o)
{
    //int PyObject_IsInstance(PyObject *inst, PyObject *cls)
    //{
        //return nullptr;
    //}
    T* self = (T*)(o);
    return self->_p;
}



template<typename T, typename U>
inline typename T::exiv2type* unpack(PyObject* self, PyObject* args, U& out)
{
    T::exiv2type* _this = unpack_self<T>(self);
    if (_this)
    {
        if (unpack_args<U>(args, out))
        {
            return _this;
        }
    }
    return nullptr;
}

template<typename T, typename U>
inline typename T::exiv2type* unpack_optional(PyObject* self, PyObject* args, U& out)
{
    T::exiv2type* _this = unpack_self<T>(self);
    if (_this)
    {
        if (unpack_args_optional<U>(args, out))
        {
            return _this;
        }
    }
    return nullptr;
}

template<typename T>
inline bool unpack_exiv2(PyObject* o, typename T::exiv2type*& out)
{
    if (!PyObject_IsInstance(o, (PyObject*)&T::type_object))
    {
        set_python_error(PyExc_TypeError, "Object should be of type '%s'", T::type_object.tp_name);
        return false;
    }
    T* self = (T*)(o);
    if (self->_const)
    {
        set_python_error(PyExc_TypeError, "Cannot use const instance of type '%s' here", T::type_object.tp_name);
        return false;
    }
    out = self->_p;
    return true;
}

template<typename T>
inline bool unpack_exiv2(PyObject* o, typename const T::exiv2type*& out)
{
    if (!PyObject_IsInstance(o, (PyObject*)&T::type_object))
    {
        set_python_error(PyExc_TypeError, "Object should be of type '%s'", T::type_object.tp_name);
        return false;
    }
    T* self = (T*)(o);
    out = self->_pconst;
    return true;
}

#if 0
template<typename T>
inline bool unpack_args(PyObject* args, T& out)
{
    error;
}
#endif
/////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
inline bool unpack_args(PyObject* args, T& out)
{
    PyObject* o;
    if (PyArg_ParseTuple(args, "O", &o))
    {
        return unpack_exiv2 < catalog <std::remove_pointer<T>::type >::pythontype > (o, out);
    }
    return false;
}

template<>
inline bool unpack_args(PyObject* args, PyObject*& out)
{
    if (PyArg_ParseTuple(args, "O", &out))
    {
        return true;
    }
    return false;
}

template<>
inline bool unpack_args(PyObject* args, long& out)
{
    if (PyArg_ParseTuple(args, "i", &out))
    {
        return true;
    }
    return false;
}

template<>
inline bool unpack_args(PyObject* args, std::string& out)
{
    const char* s;
    int nbr_bytes;
    if (PyArg_ParseTuple(args, "y#", &s, &nbr_bytes))
    {
        out.assign(s, nbr_bytes);
        return true;
    }
    return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
inline bool unpack_args_optional(PyObject* args, T& out)
{
    error;
}

template<>
inline bool unpack_args_optional(PyObject* args, long& out)
{
    if (PyArg_ParseTuple(args, "|i", &out))
    {
        return true;
    }
    return false;
}

bool unpack_bytes(PyObject* o, std::string& out);

template<typename T>
bool unpack_datum_assignment_args(typename T::exiv2type* assignee, PyObject* args)
{
    Py_ssize_t size = PyTuple_GET_SIZE(args);
    if (size != 1)
    {
        set_python_error(PyExc_TypeError, "Expected exactly one argument");
        return false;
    }
    else
    {
        PyObject* arg = PyTuple_GET_ITEM(args, 0);
        return unpack_datum_assignment<T>(assignee, arg);
    }
}

template<typename T>
bool unpack_datum_assignment(typename T::exiv2type* assignee, PyObject* arg)
{
    if (PyBytes_Check(arg))
    {
        std::string str;
        char* s;
        Py_ssize_t nbr_bytes;
        PyBytes_AsStringAndSize(arg, &s, &nbr_bytes);
        str.assign(s, nbr_bytes);
        *assignee = str;
    }
    else if (PyLong_Check(arg))
    {
        *assignee = uint16_t(PyLong_AsLong(arg));
    }
    else if (PyObject_IsInstance(arg, (PyObject*)&T::type_object))
    {
        *assignee = *(((T*)arg)->_p);
    }
    else if (PyObject_IsInstance(arg, (PyObject*)&baseclass<Exiv2::Value>::type_object))
    {
        *assignee = *(((baseclass<Exiv2::Value>*)arg)->_p);
    }
    else
    {
        set_python_error(PyExc_TypeError, "Value should be bytes, or int, or %s, or %s", T::type_object.tp_name, baseclass<Exiv2::Value>::type_object.tp_name);
        return false;
    }
    return true;
}

#if 0
template<typename T>
inline T* unpack(PyObject* o)
{
	void* ptr = PyLong_AsVoidPtr(o);
	if (PyErr_Occurred())
	{
		return nullptr;
	}
	return static_cast<T*>(ptr);
}

template<typename T>
inline T* unpack_noargs(PyObject* args)
{
	PyObject* o;
	if (!PyArg_ParseTuple(args, "O", &o))
	{
		return nullptr;
	}
	void* ptr = PyLong_AsVoidPtr(o);
	if (PyErr_Occurred())
	{
		return nullptr;
	}
	return static_cast<T*>(ptr);
}

template<typename T>
inline T* unpack_bytes(PyObject* args, std::string& out)
{
    PyObject* o;
    const char* s;
    int nbr_bytes;
    if (!PyArg_ParseTuple(args, "Oy#", &o, &s, &nbr_bytes))
    {
        return nullptr;
    }
    if (T* ptr = unpack<T>(o))
    {
        out.assign(s, nbr_bytes);
        return ptr;
    }
    return nullptr;
}

template<typename T>
inline T* unpack_string(PyObject* args, std::string& out)
{
	PyObject* o;
	const char* s;
	if (!PyArg_ParseTuple(args, "Os", &o, &s))
	{
		return nullptr;
	}
	if (T* ptr = unpack<T>(o))
	{
		out.assign(s);
		return ptr;
	}
	return nullptr;
}

template<typename T, typename U>
inline T* unpack_ptr(PyObject* args, U*& u)
{
	PyObject *o1, *o2;
	if (PyArg_ParseTuple(args, "OO", &o1, &o2))
	{
		if (T* t = unpack<T>(o1))
		{
			if (u = unpack<U>(o2))
			{
				return t;
			}
		}
	}
	return nullptr;
}

template<typename T>
inline T* unpack_long(PyObject* args, long& out)
{
	PyObject *o1;
	if (PyArg_ParseTuple(args, "Oi", &o1, &out))
	{
		if (T* t = unpack<T>(o1))
		{
			return t;
		}
	}
	return nullptr;
}

template<typename T>
inline T* unpack_optional_long(PyObject* args, long& out)
{
	PyObject *o1;
	if (PyArg_ParseTuple(args, "O|i", &o1, &out))
	{
		if (T* t = unpack<T>(o1))
		{
			return t;
		}
	}
	return nullptr;
}
#endif

