#include "precompile.h"
#include "main.h"


CLASS_METHOD_TABLE(ExifData);
CLASS_METHOD_TABLE(Exifdatum);


METHOD_VOID_NOARGS(ExifData, clear)
METHOD_VOID_NOARGS(ExifData, sortByKey)
METHOD_VOID_NOARGS(ExifData, sortByTag)

METHOD_GET_NOARGS(ExifData, empty)
METHOD_GET_NOARGS(ExifData, count)

using namespace Exiv2;

#if 0
METHOD_TABLE(ExifData)


TRAMPOLINE(get_noarg_nonconst, ExifData, begin)
TRAMPOLINE(get_noarg_nonconst, ExifData, end)

GET_NOARGS(ExifData, empty)
GET_NOARGS(ExifData, count)

VARARGS(ExifData, operator_bracket)
{
    std::string key;
    ExifData* p = unpack_bytes<ExifData>(args, key);
    if (p)
    {
        Exifdatum* datum = &(p->operator[](key));
        return PyLong_FromVoidPtr(datum);
    }
    return nullptr;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

GET_NOARGS(Exifdatum, key)
GET_NOARGS(Exifdatum, groupName)
GET_NOARGS(Exifdatum, familyName)
GET_NOARGS(Exifdatum, tagName)
GET_NOARGS(Exifdatum, tagLabel)
GET_NOARGS(Exifdatum, tag)
GET_NOARGS(Exifdatum, ifdId)
GET_NOARGS(Exifdatum, ifdName)
GET_NOARGS(Exifdatum, idx)
GET_NOARGS(Exifdatum, typeId)
GET_NOARGS(Exifdatum, typeName)
GET_NOARGS(Exifdatum, typeSize)
GET_NOARGS(Exifdatum, count)
GET_NOARGS(Exifdatum, size)
GET_NOARGS(Exifdatum, sizeDataArea)
GET_NOARGS(Exifdatum, toString)
GETREF_NOARGS(Exifdatum, value)

#if 0
long copy(byte* buf, ByteOrder byteOrder) const;
std::ostream& write(std::ostream& os, const ExifData* pMetadata = 0) const;
//! Return the value as a string.
std::string toString() const;
std::string toString(long n) const;
long toLong(long n = 0) const;
float toFloat(long n = 0) const;
Rational toRational(long n = 0) const;
Value::AutoPtr getValue() const;
const Value& value() const;
//! Return the size of the data area.
long sizeDataArea() const;
/*!
@brief Return a copy of the data area of the value. The caller owns
this copy and %DataBuf ensures that it will be deleted.

Values may have a data area, which can contain additional
information besides the actual value. This method is used to access
such a data area.

@return A %DataBuf containing a copy of the data area or an empty
%DataBuf if the value does not have a data area assigned or the
value is not set.
*/
DataBuf dataArea() const;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

VARARGS(ExifDataIterator, operator_compare)
{
    ExifData::iterator* p2;
    ExifData::iterator* p1 = unpack_ptr<ExifData::iterator, ExifData::iterator>(args, p2);
    if (p1)
    {
        return PyBool_FromLong(*p1 == *p2);
    }
    return nullptr;
}

VARARGS(ExifDataIterator, operator_increment)
{
    ExifData::iterator* p = unpack_noargs<ExifData::iterator>(args);
    if (p)
    {
        (*p)++;
        Py_RETURN_NONE;
    }
    return nullptr;
}

VARARGS(ExifDataIterator, operator_dereference)
{
    ExifData::iterator* p = unpack_noargs<ExifData::iterator>(args);
    if (p)
    {
        Exifdatum& datum = **p;
        return PyLong_FromVoidPtr(&datum);
    }
    return nullptr;
}





#if 0

TRAMPOLINE(get_noarg, ExifData, )
TRAMPOLINE(get_noarg, ExifData, )
TRAMPOLINE(get_noarg, ExifData, )
TRAMPOLINE(get_noarg, ExifData, )
TRAMPOLINE(get_noarg, ExifData, )
TRAMPOLINE(get_noarg, ExifData, )


class EXIV2API ExifData {
public:
	//! ExifMetadata iterator type
	typedef ExifMetadata::iterator iterator;
	//! ExifMetadata const iterator type
	typedef ExifMetadata::const_iterator const_iterator;

	/*!
	@brief Add an Exifdatum from the supplied key and value pair.  This
	method copies (clones) key and value. No duplicate checks are
	performed, i.e., it is possible to add multiple metadata with
	the same key.
	*/
	void add(const ExifKey& key, const Value* pValue);
	/*!
	@brief Add a copy of the \em exifdatum to the Exif metadata.  No
	duplicate checks are performed, i.e., it is possible to add
	multiple metadata with the same key.

	@throw Error if the makernote cannot be created
	*/
	void add(const Exifdatum& exifdatum);
	/*!
	@brief Delete the Exifdatum at iterator position \em pos, return the
	position of the next exifdatum. Note that iterators into
	the metadata, including \em pos, are potentially invalidated
	by this call.
	*/
	iterator erase(iterator pos);
	/*!
	@brief Remove all elements of the range \em beg, \em end, return the
	position of the next element. Note that iterators into
	the metadata are potentially invalidated by this call.
	*/
	iterator erase(iterator beg, iterator end);
	/*!
	@brief Delete all Exifdatum instances resulting in an empty container.
	Note that this also removes thumbnails.
	*/

	iterator begin() { return exifMetadata_.begin(); }
	//! End of the metadata
	iterator end() { return exifMetadata_.end(); }
	/*!
	@brief Find the first Exifdatum with the given \em key, return an
	iterator to it.
	*/
	iterator findKey(const ExifKey& key);
	//@}

	//! @name Accessors
	//@{
	//! Begin of the metadata
	const_iterator begin() const { return exifMetadata_.begin(); }
	//! End of the metadata
	const_iterator end() const { return exifMetadata_.end(); }
	/*!
	@brief Find the first Exifdatum with the given \em key, return a const
	iterator to it.
	*/
	const_iterator findKey(const ExifKey& key) const;
	//! Return true if there is no Exif metadata
	//@}
#endif
#endif
