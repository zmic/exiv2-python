#pragma once

extern PyObject* PyExc_Exiv2;
void set_python_error(PyObject* exc_type, char * format, ...);

template<typename T, typename... Args>
T exiv2_exception_catcher(T(*f)(Args...), T errorvalue, Args... args)
{
    try
    {
        return f(args...);
    }
    catch (Exiv2::AnyError& e)
    {
        set_python_error(PyExc_Exiv2, "%s", e.what());
    }
    return errorvalue;
}


#if 0
template<typename Fn, Fn fn, typename... Args>
typename std::result_of<Fn(Args...)>::type
wrapper(Args&&... args) {
    return fn(std::forward<Args>(args)...);
}
#endif


struct UNLOCK_GIL
{
    PyThreadState *_save;
    UNLOCK_GIL()
    {
        _save = PyEval_SaveThread();
    }
    ~UNLOCK_GIL()
    {
        PyEval_RestoreThread(_save);
    }
};

struct obj_ptr
{
    obj_ptr(PyObject*o) : _ptr(o) {}
    ~obj_ptr() { Py_XDECREF(_ptr); }
    PyObject* get()
    {
        return _ptr;
    }
    PyObject* release()
    {
        PyObject* r = _ptr;
        _ptr = nullptr;
        return r;
    }
    PyObject* _ptr;
};

