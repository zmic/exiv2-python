#pragma once

template <typename EXIV2TYPE>
struct baseclass
{
    typedef typename EXIV2TYPE exiv2type;

    PyObject_HEAD;
    union
    {
        EXIV2TYPE* _p;
        CONST EXIV2TYPE* _pconst;
    };
    bool        _const;
    PyObject*   _owner;

    static PyObject* alloc(EXIV2TYPE* p, PyObject* owner)
    {
        baseclass* o = (baseclass*)(type_object.tp_alloc(&type_object, 0));
        o->_p = p;
        o->_const = false;
        o->_owner = owner;
        if (owner)
        {
            Py_INCREF(owner);
        }
        return (PyObject*)o;
    }
    static PyObject* alloc(const EXIV2TYPE* p, PyObject* owner)
    {
        baseclass* o = (baseclass*)(type_object.tp_alloc(&type_object, 0));
        o->_pconst = p;
        o->_const = true;
        o->_owner = owner;
        if (owner)
        {
            Py_INCREF(owner);
        }
        return (PyObject*)o;
    }
    static void dealloc(baseclass* self)
    {
        if (self->_owner)
        {
            Py_DECREF(self->_owner);
        }
        else
        {
            delete self->_p;
        }
        Py_TYPE(self)->tp_free((PyObject*)self);
    }
    static PyTypeObject type_object;
    static std::vector<PyMethodDef> methods;
};

template<typename EXIV2TYPE>
PyObject* _new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    EXIV2TYPE* p = new EXIV2TYPE;
    return baseclass<EXIV2TYPE>::alloc(p, nullptr);
}

template<typename EXIV2TYPE, typename EXIV2TYPE_KEY>
PyObject* new_buffer_buffer(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    const char* s1;
    int n1;
    const char* s2;
    int n2;

    if (PyArg_ParseTuple(args, "y#y#", &s1, &n1, &s2, &n2))
    {
        std::string str1(s1, n1);
        std::string str2(s2, n2);
        EXIV2TYPE_KEY k(s1);
        Exiv2::StringValue v(s2);
        EXIV2TYPE* p = new EXIV2TYPE(k, &v);
        return baseclass<EXIV2TYPE>::alloc(p, nullptr);
    }
    return nullptr;
}




template<class EXIV2TYPE>
PyTypeObject baseclass<EXIV2TYPE>::type_object =
{
    PyVarObject_HEAD_INIT(NULL, 0)
    "cexiv2.xxx",                               // const char *tp_name; /* For printing, in format "<module>.<name>" */
    sizeof(baseclass),                          // Py_ssize_t tp_basicsize; /* For allocation */
    0,                                          // Py_ssize_t tp_itemsize; /* For allocation */

                                                // /* Methods to implement standard operations */

    (destructor)dealloc,                        // destructor tp_dealloc;
    0,                                          // printfunc tp_print;
    0,                                          // getattrfunc tp_getattr;
    0,                                          // setattrfunc tp_setattr;
    0,                                          // void *tp_reserved; /* formerly known as tp_compare */
    0,                                          // reprfunc tp_repr;

                                                // /* Method suites for standard classes */

    0,                                          // PyNumberMethods *tp_as_number;
    0,                                          // PySequenceMethods *tp_as_sequence;
    0,                                          // PyMappingMethods *tp_as_mapping;

                                                // /* More standard operations (here for binary compatibility) */

    0,                                          // hashfunc tp_hash;
    0,                                          // ternaryfunc tp_call;
    0,                                          // reprfunc tp_str;
    0,                                          // getattrofunc tp_getattro;
    0,                                          // setattrofunc tp_setattro;

                                                // /* Functions to access object as input/output buffer */
    0,                                          // PyBufferProcs *tp_as_buffer;

                                                // /* Flags to define presence of optional/expanded features */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,   // long tp_flags;

    0,                                          // const char *tp_doc; /* Documentation string */

                                                // /* Assigned meaning in release 2.0 */
                                                // /* call function for all accessible objects */
    0,                                          // traverseproc tp_traverse;

                                                // /* delete references to contained objects */
    0,                                          // inquiry tp_clear;

                                                // /* Assigned meaning in release 2.1 */
                                                // /* rich comparisons */
    0,                                          // richcmpfunc tp_richcompare;

                                                // /* weak reference enabler */
    0,                                          // Py_ssize_t tp_weaklistoffset;

                                                // /* Iterators */
    0,                                          // getiterfunc tp_iter;
    0,                                          // iternextfunc tp_iternext;

                                                // /* Attribute descriptor and subclassing stuff */
    0,                                          // struct PyMethodDef *tp_methods;
    0,                                          // struct PyMemberDef *tp_members;
    0,                                          // struct PyGetSetDef *tp_getset;
    0,                                          // struct _typeobject *tp_base;
    0,                                          // PyObject *tp_dict;
    0,                                          // descrgetfunc tp_descr_get;
    0,                                          // descrsetfunc tp_descr_set;
    0,                                          // Py_ssize_t tp_dictoffset;
    0,                                          // initproc tp_init;
    0,                                          // allocfunc tp_alloc;
    0,                                          // newfunc tp_new;
    0,                                          // freefunc tp_free; /* Low-level free-memory routine */
    0,                                          // inquiry tp_is_gc; /* For PyObject_IS_GC */
    0,                                          // PyObject *tp_bases;
    0,                                          // PyObject *tp_mro; /* method resolution order */
    0,                                          // PyObject *tp_cache;
    0,                                          // PyObject *tp_subclasses;
    0,                                          // PyObject *tp_weaklist;
    0,                                          // destructor tp_del;

                                                // /* Type attribute cache version tag. Added in version 2.6 */
    0                                           // unsigned int tp_version_tag;
};


struct __add_class
{
    typedef std::vector<std::tuple<const char*, PyTypeObject*, std::vector<PyMethodDef>*> > vector3;
    static vector3* classes_to_add;

    __add_class(
        const char* name, 
        PyTypeObject* type_object, 
        std::vector<PyMethodDef>* methods
    )
    {
        if (!classes_to_add)
        {
            classes_to_add = new vector3;
        }
        classes_to_add->push_back(std::make_tuple(name, type_object, methods));
    }
    static void create_classes(PyObject* module)
    {
        for (auto it : *classes_to_add)
        {
            PyTypeObject* to = std::get<1>(it);
            to->tp_name = std::get<0>(it);
            std::vector<PyMethodDef>* methods = std::get<2>(it);
            methods->push_back({ 0,0,0,0 });
            to->tp_methods = &methods->front();
            PyType_Ready(to);
            PyModule_AddObject(module, to->tp_name + 7, (PyObject*)to);
        }
    }
};

#define CLASS_METHOD_TABLE(class)\
static __add_class __x##class("cexiv2." #class, &baseclass<Exiv2::class>::type_object, &baseclass<Exiv2::class>::methods);\
std::vector<PyMethodDef> baseclass<Exiv2::class>::methods;\
template struct baseclass<Exiv2::class>;\
typedef baseclass<Exiv2::class> class;



//#pragma warning(disable:4661)



