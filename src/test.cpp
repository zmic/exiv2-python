#include "precompile.h"

namespace
{

    template<typename T>
    inline int convert(const T&)
    {}
    template<>
    inline int convert(const int& i)
    {
        return i + 1;
    }


    struct A
    {
        int _x;
        A() : _x(0) {}

        int a() { return _x; }
        int& b() { return _x; }
        const int& c() { return _x; }
    };

    template<typename T, typename PFTYPE, PFTYPE pf>
    int stuff(A& a)
    {
        typedef decltype((((T*)nullptr)->*pf)()) RETURN_TYPE;
        RETURN_TYPE r = (a.*pf)();
        r += 1;
        return convert<RETURN_TYPE>(r);
    }

#define STUFF(method) stuff<A, decltype(&A::method), &A::method>

    A A_instance;
    int r1 = STUFF(a)(A_instance);
    //int r2 = STUFF(b)(A_instance);

}
