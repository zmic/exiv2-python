#include "precompile.h"
#include "main.h"

CLASS_METHOD_TABLE(Value);

METHOD_GET_NOARGS(Value, typeId)
METHOD_GET_NOARGS(Value, count)
METHOD_GET_NOARGS(Value, size)
METHOD_GET_NOARGS(Value, sizeDataArea)
METHOD_GET_NOARGS(Value, ok)


METHOD_GET_OPTIONAL(Value, toString)
METHOD_GET_OPTIONAL(Value, toLong)
METHOD_GET_OPTIONAL(Value, toFloat)
METHOD_GET_OPTIONAL(Value, toRational)

using namespace Exiv2;


#if 0
METHOD_TABLE(Value)

template<typename T>
PyObject* f1(PyObject* self, PyObject* args, T(Value::*pf)(long) const)
{
	long n = 0;
	Value* v = unpack_optional_long<Value>(args, n);
	if (v)
	{
		T t((v->*pf)(n));
		return py_return(t);
	}
	return nullptr;
}

TRAMPOLINE(f1<std::string>, Value, toString)
TRAMPOLINE(f1<long>, Value, toLong)
TRAMPOLINE(f1<float>, Value, toFloat)
#endif


#if 0
/*!

#if 0
explicit Value(TypeId typeId);
//! Virtual destructor.
virtual ~Value();
//@}
//! @name Manipulators
//@{
/*!
@brief Read the value from a character buffer.

@param buf Pointer to the data buffer to read from
@param len Number of bytes in the data buffer
@param byteOrder Applicable byte order (little or big endian).

@return 0 if successful.
*/
virtual int read(const byte* buf, long len, ByteOrder byteOrder) = 0;
/*!
@brief Set the value from a string buffer. The format of the string
corresponds to that of the write() method, i.e., a string
obtained through the write() method can be read by this
function.

@param buf The string to read from.

@return 0 if successful.
*/
virtual int read(const std::string& buf) = 0;
/*!
@brief Set the data area, if the value has one by copying (cloning)
the buffer pointed to by buf.

Values may have a data area, which can contain additional
information besides the actual value. This method is used to set such
a data area.

@param buf Pointer to the source data area
@param len Size of the data area
@return Return -1 if the value has no data area, else 0.
*/
virtual int setDataArea(const byte* buf, long len);
//@}

//! @name Accessors
//@{
//! Return the type identifier (Exif data format type).
TypeId typeId() const { return type_; }
/*!
@brief Return an auto-pointer to a copy of itself (deep copy).
The caller owns this copy and the auto-pointer ensures that
it will be deleted.
*/
AutoPtr clone() const { return AutoPtr(clone_()); }
/*!
@brief Write value to a data buffer.

The user must ensure that the buffer has enough memory. Otherwise
the call results in undefined behaviour.

@param buf Data buffer to write to.
@param byteOrder Applicable byte order (little or big endian).
@return Number of bytes written.
*/
virtual long copy(byte* buf, ByteOrder byteOrder) const = 0;
/*!
@brief Write the value to an output stream. You do not usually have
to use this function; it is used for the implementation of
the output operator for %Value,
operator<<(std::ostream &os, const Value &value).
*/
virtual std::ostream& write(std::ostream& os) const = 0;
/*!
@brief Return the value as a string. Implemented in terms of
write(std::ostream& os) const of the concrete class.
*/
std::string toString() const;
/*!
@brief Return the <EM>n</EM>-th component of the value as a string.
The default implementation returns toString(). The behaviour
of this method may be undefined if there is no <EM>n</EM>-th
component.
*/
virtual std::string toString(long n) const;
/*!
@brief Convert the <EM>n</EM>-th component of the value to a long.
The behaviour of this method may be undefined if there is no
<EM>n</EM>-th component.

@return The converted value.
*/
virtual long toLong(long n = 0) const = 0;
/*!
@brief Convert the <EM>n</EM>-th component of the value to a float.
The behaviour of this method may be undefined if there is no
<EM>n</EM>-th component.

@return The converted value.
*/
virtual float toFloat(long n = 0) const = 0;
/*!
@brief Convert the <EM>n</EM>-th component of the value to a Rational.
The behaviour of this method may be undefined if there is no
<EM>n</EM>-th component.

@return The converted value.
*/
virtual Rational toRational(long n = 0) const = 0;
/*!
@brief Return a copy of the data area if the value has one. The
caller owns this copy and DataBuf ensures that it will be
deleted.

Values may have a data area, which can contain additional
information besides the actual value. This method is used to access
such a data area.

@return A DataBuf containing a copy of the data area or an empty
DataBuf if the value does not have a data area assigned.
*/
virtual DataBuf dataArea() const;
/*!
@brief Check the \em ok status indicator. After a to<Type> conversion,
this indicator shows whether the conversion was successful.
*/
bool ok() const { return ok_; }
//@}

/*!
@brief A (simple) factory to create a Value type.



@param typeId Type of the value.
@return Auto-pointer to the newly created Value. The caller owns this
copy and the auto-pointer ensures that it will be deleted.
*/
static AutoPtr create(TypeId typeId);
#endif
