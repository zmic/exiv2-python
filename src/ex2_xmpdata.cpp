#include "precompile.h"
#include "main.h"


CLASS_METHOD_TABLE(XmpData);
CLASS_METHOD_TABLE(Xmpdatum);


template<PyObject*(*func)(PyObject*)>
PyObject* exiv2_catcher__(PyObject* self)
{
    PyObject* result = nullptr;
    try
    {
        result = func(self);
    }
    catch (Exiv2::AnyError& e)
    {
        set_python_error(PyExc_Exiv2, "%s", e.what());
    }
    return result;
}

///////////////////////////////////////////////////////////////////////
template<typename T, void(T::exiv2type::*pf)(void)>
struct wrapped_method__void_noargs
{
    static PyObject* method_void_noargs__(PyObject* self)
    {
        T::exiv2type* _this = unpack_self<T>(self);
        if (_this)
        {
            (_this->*pf)();
            Py_RETURN_NONE;
        }
        return nullptr;
    }

    wrapped_method__void_noargs(const char* pyname)
    {
        push_NOARGS(T::methods, pyname, exiv2_catcher__<method_void_noargs__>);
    }
};


///////////////////////////////////////////////////////////////////////
template<typename T>
struct wrapped_method__noargs
{
    //typedef typename catalog<T>::pythontype pythontype;

    template<typename U, U(T::exiv2type::*pf)(void) const>
    static PyObject* method_noargs2(PyObject* self)
    {
        /*
        T* _this = unpack_self<pythontype>(self);
        if (_this)
        {
            U u = (_this->*pf)();
            return py_return<U>(u);
        }
        */
        return nullptr;
    }

    template<typename R, R(T::exiv2type::*pf)(void) const>
    wrapped_method__noargs(const char* pyname, R(T::exiv2type::*pf2)(void) const)
    {
        //method_noargs2<R, pf>(nullptr);
        //push_NOARGS(T::methods, pyname, exiv2_catcher__< >);
    }
};

//wrapped_method__noargs<XmpData> a2("aa", &XmpData::exiv2type::count);

template<typename T, typename U, U(T::*pf)(void)>
int wrapped_method__get_noargs__(const char* pyname, U(T::*)(void) const)
{
    //auto pf2 = exiv2_catcher__<method_get_noargs__<pf> >;
    //push_NOARGS(pythontype::methods, pyname, pf2);
    return 0;
}

template<typename T, typename R>
PyObject* method_set1(PyObject* self, PyObject* args, R(T::exiv2type::*pf)(void) const)
{
    return nullptr;
}

wrapped_method__void_noargs<XmpData, &XmpData::exiv2type::clear> a("aa");
//int i = wrapped_method__get_noargs__<XmpData::exiv2type>("aa", &XmpData::exiv2type::count);
//PyObject* o = method_set1<XmpData>(nullptr, nullptr, &XmpData::exiv2type::count);

//METHOD_VOID_NOARGS(XmpData, clear)

#if 0
VOID_NOARGS(XmpData, clear)
VOID_NOARGS(XmpData, sortByKey)
VOID_NOARGS(XmpData, sortByTag)

TRAMPOLINE(get_noarg_nonconst, XmpData, begin)
TRAMPOLINE(get_noarg_nonconst, XmpData, end)

GET_NOARGS(XmpData, empty)
GET_NOARGS(XmpData, count)

VARARGS(XmpData, operator_bracket)
{
    std::string key;
    XmpData* p = unpack_bytes<XmpData>(args, key);
    if (p)
    {
        Xmpdatum* datum = &(p->operator[](key));
        return PyLong_FromVoidPtr(datum);
    }
    return nullptr;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

GET_NOARGS(Xmpdatum, key)
GET_NOARGS(Xmpdatum, groupName)
GET_NOARGS(Xmpdatum, familyName)
GET_NOARGS(Xmpdatum, tagName)
GET_NOARGS(Xmpdatum, tagLabel)
GET_NOARGS(Xmpdatum, tag)
GET_NOARGS(Xmpdatum, ifdId)
GET_NOARGS(Xmpdatum, ifdName)
GET_NOARGS(Xmpdatum, idx)
GET_NOARGS(Xmpdatum, typeId)
GET_NOARGS(Xmpdatum, typeName)
GET_NOARGS(Xmpdatum, typeSize)
GET_NOARGS(Xmpdatum, count)
GET_NOARGS(Xmpdatum, size)
GET_NOARGS(Xmpdatum, sizeDataArea)
GET_NOARGS(Xmpdatum, toString)
GETREF_NOARGS(Xmpdatum, value)

#if 0
long copy(byte* buf, ByteOrder byteOrder) const;
std::ostream& write(std::ostream& os, const XmpData* pMetadata = 0) const;
//! Return the value as a string.
std::string toString() const;
std::string toString(long n) const;
long toLong(long n = 0) const;
float toFloat(long n = 0) const;
Rational toRational(long n = 0) const;
Value::AutoPtr getValue() const;
const Value& value() const;
//! Return the size of the data area.
long sizeDataArea() const;
/*!
@brief Return a copy of the data area of the value. The caller owns
this copy and %DataBuf ensures that it will be deleted.

Values may have a data area, which can contain additional
information besides the actual value. This method is used to access
such a data area.

@return A %DataBuf containing a copy of the data area or an empty
%DataBuf if the value does not have a data area assigned or the
value is not set.
*/
DataBuf dataArea() const;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

VARARGS(XmpDataIterator, operator_compare)
{
    XmpData::iterator* p2;
    XmpData::iterator* p1 = unpack_ptr<XmpData::iterator, XmpData::iterator>(args, p2);
    if (p1)
    {
        return PyBool_FromLong(*p1 == *p2);
    }
    return nullptr;
}

VARARGS(XmpDataIterator, operator_increment)
{
    XmpData::iterator* p = unpack_noargs<XmpData::iterator>(args);
    if (p)
    {
        (*p)++;
        Py_RETURN_NONE;
    }
    return nullptr;
}

VARARGS(XmpDataIterator, operator_dereference)
{
    XmpData::iterator* p = unpack_noargs<XmpData::iterator>(args);
    if (p)
    {
        Xmpdatum& datum = **p;
        return PyLong_FromVoidPtr(&datum);
    }
    return nullptr;
}





#if 0

TRAMPOLINE(get_noarg, XmpData, )
TRAMPOLINE(get_noarg, XmpData, )
TRAMPOLINE(get_noarg, XmpData, )
TRAMPOLINE(get_noarg, XmpData, )
TRAMPOLINE(get_noarg, XmpData, )
TRAMPOLINE(get_noarg, XmpData, )


class EXIV2API XmpData {
public:
	//! ExifMetadata iterator type
	typedef ExifMetadata::iterator iterator;
	//! ExifMetadata const iterator type
	typedef ExifMetadata::const_iterator const_iterator;

	/*!
	@brief Add an Xmpdatum from the supplied key and value pair.  This
	method copies (clones) key and value. No duplicate checks are
	performed, i.e., it is possible to add multiple metadata with
	the same key.
	*/
	void add(const ExifKey& key, const Value* pValue);
	/*!
	@brief Add a copy of the \em Xmpdatum to the Exif metadata.  No
	duplicate checks are performed, i.e., it is possible to add
	multiple metadata with the same key.

	@throw Error if the makernote cannot be created
	*/
	void add(const Xmpdatum& Xmpdatum);
	/*!
	@brief Delete the Xmpdatum at iterator position \em pos, return the
	position of the next Xmpdatum. Note that iterators into
	the metadata, including \em pos, are potentially invalidated
	by this call.
	*/
	iterator erase(iterator pos);
	/*!
	@brief Remove all elements of the range \em beg, \em end, return the
	position of the next element. Note that iterators into
	the metadata are potentially invalidated by this call.
	*/
	iterator erase(iterator beg, iterator end);
	/*!
	@brief Delete all Xmpdatum instances resulting in an empty container.
	Note that this also removes thumbnails.
	*/

	iterator begin() { return exifMetadata_.begin(); }
	//! End of the metadata
	iterator end() { return exifMetadata_.end(); }
	/*!
	@brief Find the first Xmpdatum with the given \em key, return an
	iterator to it.
	*/
	iterator findKey(const ExifKey& key);
	//@}

	//! @name Accessors
	//@{
	//! Begin of the metadata
	const_iterator begin() const { return exifMetadata_.begin(); }
	//! End of the metadata
	const_iterator end() const { return exifMetadata_.end(); }
	/*!
	@brief Find the first Xmpdatum with the given \em key, return a const
	iterator to it.
	*/
	const_iterator findKey(const ExifKey& key) const;
	//! Return true if there is no Exif metadata
	//@}
#endif
#endif
