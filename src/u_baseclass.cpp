#include "precompile.h"
#include "u_baseclass.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__add_class::vector3* __add_class::classes_to_add;

#if 0

#define ADD_CLASS(module, class)\
ADD_CLASS__(module, class, class)

#define ADD_CLASS__(module, class, classname)\
add_class<class>(module, #classname, "cexiv2." #class)

template<typename T>
void add_class(PyObject* module, const char* classname, const char* tp_name)
{
    PyTypeObject& to = T::type_object;
    to.tp_name = tp_name;
    T::methods.push_back({ 0,0,0,0 });
    to.tp_methods = &T::methods.front();
    PyType_Ready(&to);
    PyModule_AddObject(module, classname, (PyObject*)&to);
}


//PyObject* create_types(PyObject* self, PyObject* args)
void create_classes(PyObject* module)
{
    //PyObject* module = PyTuple_GET_ITEM(args, 0);
    ADD_CLASS(module, Image);
}


#endif
