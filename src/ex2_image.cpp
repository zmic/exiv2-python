#include "precompile.h"
#include "main.h"

CLASS_METHOD_TABLE(Image);

typedef Exiv2::Image xxx;

METHOD_VOID_NOARGS(Image, readMetadata);
METHOD_VOID_NOARGS(Image, writeMetadata);
METHOD_VOID_NOARGS(Image, clearExifData);
METHOD_VOID_NOARGS(Image, clearIptcData);
METHOD_VOID_NOARGS(Image, clearXmpPacket);
METHOD_VOID_NOARGS(Image, clearXmpData);
METHOD_VOID_NOARGS(Image, clearComment);
METHOD_VOID_NOARGS(Image, clearIccProfile);
METHOD_VOID_NOARGS(Image, clearMetadata);

METHOD_GET_NOARGS(Image, good);
METHOD_GET_NOARGS(Image, isLittleEndianPlatform);
METHOD_GET_NOARGS(Image, pixelWidth);
METHOD_GET_NOARGS(Image, pixelHeight);

METHOD_GETREF_NOARGS_NONCONST(Image, exifData);
METHOD_GETREF_NOARGS_NONCONST(Image, iptcData);
METHOD_GETREF_NOARGS_NONCONST(Image, xmpData);

//METHOD_GETREF_NOARGS_NONCONST(Image, xmpData);

METHOD_GETREF_NOARGS_CONST(Image, io);

VOID_METHOD_SET(Image, setExifData)
VOID_METHOD_SET(Image, setIptcData)
VOID_METHOD_SET(Image, setXmpData)

using namespace Exiv2;

#if 0

template<typename T>
PyObject* f1(PyObject* self, PyObject* args, void(Image::*pf)(const T&))
{
	T* t;
	Image* im = unpack_ptr<Image, T>(args, t);
	if (im)
	{
		(im->*pf)(*t);
		Py_RETURN_NONE;
	}
	return nullptr;
}

TRAMPOLINE(f1<ExifData>, Image, setExifData)
TRAMPOLINE(f1<IptcData>, Image, setIptcData)
TRAMPOLINE(f1<XmpData>, Image, setXmpData)


/////////////////////////////////////////////////////////////////////////////////////////////
VARARGS(Image, setMetadata)
{
	Image* other;
	Image* i = unpack_ptr<Image, Image>(args, other);
	if (i)
	{
		i->setMetadata(*other);
		Py_RETURN_NONE;
	}
	return nullptr;
}
#endif



/*
virtual std::string& xmpPacket();
virtual void setXmpPacket(const std::string& xmpPacket);
virtual void setComment(const std::string& comment);
virtual void setMetadata(const Image& image);
virtual const ExifData& exifData() const;
virtual const IptcData& iptcData() const;
virtual const XmpData& xmpData() const;
virtual std::string comment() const;
virtual const std::string& xmpPacket() const;
virtual BasicIo& io() const;
*/
