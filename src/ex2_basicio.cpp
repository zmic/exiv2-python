#include "precompile.h"
#include "main.h"

CLASS_METHOD_TABLE(BasicIo);


METHOD_GET_NOARGS(BasicIo, open)
METHOD_GET_NOARGS(BasicIo, close)
METHOD_GET_NOARGS(BasicIo, size)
METHOD_GET_NOARGS(BasicIo, isopen)
METHOD_GET_NOARGS(BasicIo, tell)
METHOD_GET_NOARGS(BasicIo, error)
METHOD_GET_NOARGS(BasicIo, eof)
METHOD_GET_NOARGS(BasicIo, path)


/////////////////////////////////////////////////////////////////////////////////////////////

METHOD_VARARGS(BasicIo, read)
{
	long rcount;
    Exiv2::BasicIo* io = unpack<BasicIo, long>(self, args, rcount);
	if (io)
	{
		obj_ptr bytes(PyBytes_FromStringAndSize(nullptr, rcount));
		Exiv2::byte* buf = (Exiv2::byte*)PyBytes_AS_STRING(bytes.get());
		long read;
		{
			UNLOCK_GIL unlock;
			read = io->read(buf, rcount);
		}
		if (read != rcount)
		{
			PyObject* bytes2 = PyBytes_FromStringAndSize(nullptr, read);
            Exiv2::byte* buf2 = (Exiv2::byte*)PyBytes_AS_STRING(bytes2);
			memcpy(buf2, buf, read);
			bytes.release();
			return bytes2;
		}
		return bytes.release();
	}
	return nullptr;
}

/////////////////////////////////////////////////////////////////////////////////////////////

