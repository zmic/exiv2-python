#pragma once


#include "u_util.h"
#include "u_unpack.h"
#include "u_iter.h"
#include "u_baseclass.h"
#include "u_catalog.h"
#include "u_wrappers.h"
#include "u_return.h"


Py_ssize_t exiv2_catcher(Py_ssize_t(__cdecl *)(PyObject *), PyObject* self);
PyObject* exiv2_catcher(PyNoArgsFunction, PyObject* self);
PyObject* exiv2_catcher(PyCFunction, PyObject* self, PyObject* args);
int exiv2_catcher(int (__cdecl *)(PyObject *, PyObject *, PyObject *), PyObject* self, PyObject* obj, PyObject* args);




#define METHOD_TABLE(k)\
std::vector<PyMethodDef> g_methods_##k;\
namespace {\
std::vector<PyMethodDef> &g_methods = g_methods_##k;\
}; \

////////////////////////////////////////////////////////////////////////////////////////////////////

//int push_NOARGS(std::vector<PyMethodDef>& methods, const char* name, PyObject* (*pf)(PyObject*), const char* docstring);

#define PUSH_NOARGS____(cname, cfunc, pyname)\
namespace PYNS_##cname\
{\
	auto lam = [](PyObject* self) -> PyObject* { return exiv2_catcher(cfunc, self); };\
    char i = push_NOARGS( g_methods, #pyname, lam );\
}

#define PUSH_NOARGS__(cfunc, pyname)\
PUSH_NOARGS____(cfunc, cfunc, pyname)

#define NOARGS(klass, method)\
PyObject* py_##klass##_##method(PyObject* self);\
PUSH_NOARGS__(py_##klass##_##method, klass##_##method)\
PyObject* py_##klass##_##method(PyObject* self)

#define DEFINE_NEW(klass)\
NOARGS(klass, new)\
{\
return datatype_new<klass>();\
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//int push_VARARGS(std::vector<PyMethodDef>& methods, const char* name, PyObject* (*pf)(PyObject*, PyObject*));

#define PUSH_VARARGS____(cname, cfunc, pname)\
namespace PYNS_##cname\
{\
	auto lam = [](PyObject* self, PyObject* args) -> PyObject* { return exiv2_catcher(cfunc, self, args); };\
    char i = push_VARARGS(g_methods, #pname, lam );\
}

#define PUSH_VARARGS__(cfunc, pname)\
PUSH_VARARGS____(cfunc, cfunc, pname)

#define VARARGS(klass, method)\
PyObject* py_##klass##_##method(PyObject* self, PyObject* args);\
PUSH_VARARGS__(py_##klass##_##method, klass##_##method)\
PyObject* py_##klass##_##method(PyObject* self, PyObject* args)

#if 0

#define TRAMPOLINE(fct, klass, method)\
VARARGS(klass, method)\
{\
    return fct(self, args, &klass::method);\
}

#define DEFINE_DELETE(class)\
VARARGS(class, delete)\
{\
return datatype_delete<class>(args);\
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename datatype>
PyObject* datatype_new()
{
	datatype* ptr = new datatype;
	return PyLong_FromVoidPtr(ptr);
}

template<typename datatype>
PyObject* datatype_delete(PyObject* args)
{
	datatype* ptr = unpack_noargs<datatype>(args);
	if (ptr)
	{
		delete ptr;
		Py_RETURN_NONE;
	}
	return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename CLASS, void(CLASS::*pf)(void)>
PyObject* void_noargs(PyObject* self, PyObject* args)
{
    CLASS* p = unpack_noargs<CLASS>(args);
	if (p)
	{
		(p->*pf)();
		Py_RETURN_NONE;
	}
	return nullptr;
}

#define VOID_NOARGS__(klass, method) void_noargs<klass, &klass::method>
#define VOID_NOARGS(klass, method)\
PUSH_VARARGS____(klass##_##method, VOID_NOARGS__(klass, method), klass##_##method)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename CLASS, typename RETURNTYPE, RETURNTYPE(CLASS::*pf)(void)>
PyObject* get_noargs(PyObject* self, PyObject* args)
{
    CLASS* p = unpack_noargs<CLASS>(args);
    if (p)
    {
        RETURNTYPE r = (p->*pf)();
        return py_return<RETURNTYPE>(r);
    }
    return nullptr;
}

template<typename CLASS, typename RETURNTYPE, RETURNTYPE(CLASS::*pf)(void)const>
PyObject* get_noargs(PyObject* self, PyObject* args)
{
    CLASS* p = unpack_noargs<CLASS>(args);
    if (p)
    {
        RETURNTYPE r = (p->*pf)();
        return py_return<RETURNTYPE>(r);
    }
    return nullptr;
}

#define GET_NOARGS__(klass, returntype, method) get_noargs<klass, returntype, &klass::method>

#define GET_NOARGS(klass, method)\
typedef decltype(((klass*)nullptr)->method()) klass##method##returntype;\
PUSH_VARARGS____(klass##_##method, GET_NOARGS__(klass, klass##method##returntype, method), klass##_##method)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename CLASS, typename RETURNTYPE, RETURNTYPE&(CLASS::*pf)(void)>
PyObject* getref_noargs(PyObject* self, PyObject* args)
{
    CLASS* p = unpack_noargs<CLASS>(args);
    if (p)
    {
        RETURNTYPE& r = (p->*pf)();        
        return py_return<RETURNTYPE>(r);
    }
    return nullptr;
}

template<typename CLASS, typename RETURNTYPE, RETURNTYPE&(CLASS::*pf)(void)const>
PyObject* getref_noargs(PyObject* self, PyObject* args)
{
    CLASS* p = unpack_noargs<CLASS>(args);
    if (p)
    {
        RETURNTYPE& r = (p->*pf)();
        return py_return<RETURNTYPE>(r);
    }
    return nullptr;
}

#define GETREF_NOARGS__(klass, returntype, method) getref_noargs<klass, returntype, &klass::method>

#define GETREF_NOARGS(klass, method)\
typedef std::remove_reference<decltype(((klass*)nullptr)->method())>::type klass##method##returntype;\
PUSH_VARARGS____(klass##_##method, GETREF_NOARGS__(klass, klass##method##returntype, method), klass##_##method)

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename klass, typename T>
PyObject* get_noarg(PyObject* self, PyObject* args, T(klass::*pf)(void) const)
{
	klass* p = unpack_noargs<klass>(args);
	if (p)
	{
		T t = (p->*pf)();
		return py_return<T>(t);
	}
	return nullptr;
}

template<typename klass, typename T>
PyObject* get_noarg(PyObject* self, PyObject* args, T(klass::*pf)(void))
{
	klass* p = unpack_noargs<klass>(args);
	if (p)
	{
		T t = (p->*pf)();
		return py_return<T>(t);
	}
	return nullptr;
}

template<typename klass, typename T>
inline PyObject* get_noarg_nonconst(PyObject* self, PyObject* args, T(klass::*pf)(void))
{
	return get_noarg(self, args, pf);
}

#endif
