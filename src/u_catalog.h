template<typename EXIV2TYPE>
struct __catalog
{
    typedef EXIV2TYPE exivtype;
    typedef std::remove_const<EXIV2TYPE> exivtype_nonconst;
    typedef baseclass<exivtype_nonconst> pythontype;
    PyTypeObject* typeobject()
    {
        return &pythontype::pythontype;
    }
};

template<typename EXIV2TYPE>
struct catalog : public __catalog<EXIV2TYPE>
{
};


template<>
struct catalog<const Exiv2::IptcData>
{
    typedef baseclass<Exiv2::IptcData> pythontype;

};

template<>
struct catalog<const Exiv2::XmpData>
{
    typedef baseclass<Exiv2::XmpData> pythontype;

};

template<>
struct catalog<const Exiv2::ExifData>
{
    typedef baseclass<Exiv2::ExifData> pythontype;

};

template<>
struct catalog<const Exiv2::Iptcdatum>
{
    typedef baseclass<Exiv2::Iptcdatum> pythontype;

};





