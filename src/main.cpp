#include "precompile.h"
#include "main.h"


int push_NOARGS(std::vector<PyMethodDef>& methods, const char* name, PyObject* (*pf)(PyObject*), const char* docstring)
{
	PyMethodDef a = { name, (PyCFunction)pf, METH_NOARGS, docstring };
	methods.push_back(a);
	return 0;
}

int push_VARARGS(std::vector<PyMethodDef>& methods, const char* name, PyObject* (*pf)(PyObject*, PyObject*), const char* docstring)
{
	PyMethodDef a = { name, (PyCFunction)pf, METH_VARARGS, docstring };
	methods.push_back(a);
	return 0;
}

void set_python_error(PyObject* exc_type, char * format, ...)
{
	char buffer[512];
	va_list args;
	va_start(args, format);
	vsnprintf(buffer, sizeof(buffer)-1, format, args);
	PyErr_SetString(exc_type, buffer);
	va_end(args);
}



#if 0
PyObject* set_iptc_caption(PyObject* self, PyObject* args)
{
	const Exiv2::byte* data1;
	long size1;
	const char* data2;
	long size2;

	if (!PyArg_ParseTuple(args, "y#y#", &data1, &size1, &data2, &size2))
	{
		goto error;
	}
	try
	{
		_UNLOCK_GIL _ulck;
		Exiv2::Image::AutoPtr im = Exiv2::ImageFactory::open(data1, size1);
		im->readMetadata();
		im->clearMetadata();
		im->writeMetadata();

		Exiv2::IptcData iptcData;
		std::string caption(data2, size2);
		iptcData["Iptc.Application2.Caption"] = caption;
		im->setIptcData(iptcData);

		/*
		Exiv2::ExifData exifData;
		exifData["Exif.Image.ImageDescription"] = "ik";
		exifData["Exif.Image.Software"] = "ik";
		im->setExifData(exifData);

		Exiv2::XmpData xmpData;
		xmpData["Xmp.dc.source"] = "xmpsample.cpp";    // a simple text value
		xmpData["Xmp.dc.subject"] = "Palmtree";         // an array item
		xmpData["Xmp.dc.subject"] = "Rubbertree";       // add a 2nd array item
		im->setXmpData(xmpData);
		*/

		im->writeMetadata();
		Exiv2::BasicIo& io = im->io();
		unsigned size = io.size();
		PyObject* outbytes = PyBytes_FromStringAndSize(0, size);
		io.read((Exiv2::byte *)(PyBytes_AS_STRING(outbytes)), size);
		return outbytes;

	}
	catch (Exiv2::AnyError& e)
	{
		set_python_error(PyExc_RuntimeError, "Exiv2 error: %s", e.what());
		return 0;
	}
error:
	return 0;
}
#endif

PyObject* PyExc_Exiv2;

PyObject* cexiv2_set_exception_type(PyObject* self, PyObject* args)
{
	PyArg_ParseTuple(args, "O", &PyExc_Exiv2);
	Py_RETURN_NONE;
}

Py_ssize_t exiv2_catcher(Py_ssize_t(*func)(PyObject *), PyObject* self)
{
    Py_ssize_t result = -1;
    try
    {
        result = func(self);
    }
    catch (Exiv2::AnyError& e)
    {
        set_python_error(PyExc_Exiv2, "%s", e.what());
    }
    return result;
}

PyObject* exiv2_catcher(PyNoArgsFunction func, PyObject* self)
{
    PyObject* result = nullptr;
    try
    {
        result = func(self);
    }
    catch (Exiv2::AnyError& e)
    {
        set_python_error(PyExc_Exiv2, "%s", e.what());
    }
    return result;
}

PyObject* exiv2_catcher(PyCFunction func, PyObject* self, PyObject* args)
{
	PyObject* result = nullptr;
	try
	{
		result = func(self, args);
	}
	catch(Exiv2::AnyError& e)
	{
		set_python_error(PyExc_Exiv2, "%s", e.what());
	}
	return result;
}

int exiv2_catcher(int(__cdecl *func)(PyObject *, PyObject *, PyObject *), PyObject* self, PyObject* obj, PyObject* args)
{
    int result = -1;
    try
    {
        result = func(self, obj, args);
    }
    catch (Exiv2::AnyError& e)
    {
        set_python_error(PyExc_Exiv2, "%s", e.what());
    }
    return result;
}


///////////////////////////////////////////////////////////////////////////////////


#define ADD_FUNCTION(x)\
do {\
PyObject* x(PyObject* self, PyObject* args);\
auto lam = [](PyObject* self, PyObject* args) -> PyObject* { return exiv2_catcher(x, self, args); };\
PyMethodDef def = {#x,lam,METH_VARARGS,0};\
 g_methods.push_back(def);\
} while(0)

std::vector<PyMethodDef> g_methods;

void fill_methods()
{
	extern std::vector<PyMethodDef> g_methods_ImageFactory;

	//extern std::vector<PyMethodDef> g_methods_Image;
	//extern std::vector<PyMethodDef> g_methods_IptcData;
	//extern std::vector<PyMethodDef> g_methods_Iptcdatum;
	//extern std::vector<PyMethodDef> g_methods_BasicIo;
	//extern std::vector<PyMethodDef> g_methods_Value;
    //extern std::vector<PyMethodDef> g_methods_ExifData;
    
	g_methods.insert(g_methods.begin(), g_methods_ImageFactory.begin(), g_methods_ImageFactory.end());
	//g_methods.insert(g_methods.begin(), g_methods_Image.begin(), g_methods_Image.end());
	//g_methods.insert(g_methods.begin(), g_methods_IptcData.begin(), g_methods_IptcData.end());
	//g_methods.insert(g_methods.begin(), g_methods_Iptcdatum.begin(), g_methods_Iptcdatum.end());
	//g_methods.insert(g_methods.begin(), g_methods_BasicIo.begin(), g_methods_BasicIo.end());
	//g_methods.insert(g_methods.begin(), g_methods_Value.begin(), g_methods_Value.end());
    //g_methods.insert(g_methods.begin(), g_methods_ExifData.begin(), g_methods_ExifData.end());

	ADD_FUNCTION(cexiv2_set_exception_type);	
	PyMethodDef def = { 0,0,0,0 };
	g_methods.push_back(def);
}



static struct PyModuleDef moduledef = {
	PyModuleDef_HEAD_INIT,
	"cexiv2",   /* name of module */
	0, /* module documentation, may be NULL */
	-1,       /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
	nullptr
};

PyMODINIT_FUNC
PyInit_cexiv2(void)
{
	fill_methods();
	moduledef.m_methods = &g_methods[0];

	PyObject *m;
	m = PyModule_Create(&moduledef);
	if (m == NULL)
		return NULL;
    __add_class::create_classes(m);

    {
        PyTypeObject* to = &iterator<Exiv2::ExifData::iterator>::type_object;
        to->tp_name = "cexiv2.exifdata_iterator";
        PyType_Ready(to);
        PyModule_AddObject(m, to->tp_name + 7, (PyObject*)to);
    }

    {
        PyTypeObject* to = &iterator<Exiv2::IptcData::iterator>::type_object;
        to->tp_name = "cexiv2.iptcdata_iterator";
        PyType_Ready(to);
        PyModule_AddObject(m, to->tp_name + 7, (PyObject*)to);
    }

    {
        PyTypeObject* to = &iterator<Exiv2::XmpData::iterator>::type_object;
        to->tp_name = "cexiv2.xmpdata_iterator";
        PyType_Ready(to);
        PyModule_AddObject(m, to->tp_name + 7, (PyObject*)to);
    }

	return m;
}


